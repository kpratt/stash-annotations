"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
var react_redux_1 = require("react-redux");
var redux_1 = require("redux");
var reducers_1 = require("./reducers");
var app_1 = require("./app");
window.onload = function () {
    var store = redux_1.createStore(reducers_1.default);
    ReactDOM.render(<react_redux_1.Provider store={store}>
            <app_1.App />
        </react_redux_1.Provider>, document.getElementById("body"));
    store.dispatch(app_1.init());
};
