import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as mockery from 'mockery';

Enzyme.configure({ adapter: new Adapter() });
global.fetch = require('fetch-mock');
mockery.registerSubstitute('main/indirection/environment', 'main/indirection/testing');