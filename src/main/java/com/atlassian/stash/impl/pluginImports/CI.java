package com.atlassian.stash.impl.pluginImports;

import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

@Scanned
public class CI {
    @ComponentImport
    PermissionService ps;
    @ComponentImport
    PermissionValidationService pvs;
}
