"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Enzyme = require("enzyme");
var Adapter = require("enzyme-adapter-react-16");
var mockery = require("mockery");
Enzyme.configure({ adapter: new Adapter() });
global.fetch = require('fetch-mock');
mockery.registerSubstitute('main/indirection/environment', 'main/indirection/testing');
