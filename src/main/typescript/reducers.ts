import { combineReducers } from 'redux'

const rootReducer = combineReducers({
    noop
});

export type Action = null;
export type State = { cake: string };

export const initialState: State = { cake: "let them eat it" };

function noop(state: State = initialState, action: Action): State {
    return state || initialState || { cake: "is bad m'kay"};
}

export default rootReducer
