class Stash {

    host: string;
    port: string;


    constructor(host: string, port: string = "80") {
        this.host = host;
        this.port = port;
    }

    repo(project: string, repo: string): StashRepo {
        return new StashRepo(project, repo, this);
    }

    urlPrefix(): string {
        return `https://${this.host}:${this.port}/bitbucket`;
    }

};

export type NavLink = {
    label: string,
    href: string,
}

export type App = {
    label: string,
    href: string,
}

export type Annotation = {
    offset: number,
    length: number,
    url: string,
}

class StashRepo {
    project: string;
    repo: string;
    stash: Stash;

    constructor(project: string, repo: string, stash: Stash) {
        this.project = project;
        this.repo = repo;
        this.stash = stash;
    }

    urlPrefix(): string {
        return `${this.stash.urlPrefix()}/plugins/servlet/annotated-view`
    }

    // Standard BB api
    raw(ref: string, file: string): Promise<string> {
        if (file[0] == '/') {
            file = file.substring(1);
        }
        return fetch(`${this.stash.urlPrefix()}/projects/${this.project}/repos/${this.repo}/raw/${file}?at=${ref}`)
            .then((resp) => resp.text());
    }

    appSwitcher(): Promise<App[]> {
        return fetch(`${this.stash.urlPrefix()}/rest/menu/latest/appswitcher`)
            .then((resp) => resp.json());
    }

    // OUR api
    annotationsFor(ref: string, file: string): Promise<Annotation[]> {
        if (file[0] == '/') {
            file = file.substring(1);
        }
        return fetch(`${this.urlPrefix()}/projects/${this.project}/repo/${this.repo}/annotations/${file}?at=${ref}`)
            .then((resp) => resp.json());
    }

    navigation(): Promise<NavLink[]> {
        return fetch(`${this.urlPrefix()}/navItems`)
            .then((resp) => resp.json());
    }
}

const statics = {
    connect: function (host: string, port: string): Stash {
        return new Stash(host, port);
    }
}

export default statics;
