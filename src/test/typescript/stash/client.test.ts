import Stash, {Annotation} from 'main/stash/client';
import testEnv from 'main/indirection/testing';
import { deepEqual } from 'assert';
describe("Stash Client", () => {

    afterEach(fetch.restore);

    it('Fetch is mocked.', (done) => {
        const jsonResponse = {
            cake: 'eat it you will',
        };
        fetch.get(
            'https://localhost',
            {
                status: 200,
                body: jsonResponse,
            }
        );
        fetch('https://localhost').
        then((resp) => resp.json()).
        then((json) => {
            deepEqual(jsonResponse, json);
            done();
        }).catch(done.fail);
    });

    it('Fetches annotations for repo file.', (done) => {
        const jsonResponse = {
            cake: 'eat it you will',
        };
        fetch.get(
            'https://localhost:7990/bitbucket/plugins/servlet/annotated-view/projects/project_1/repo/mycode/annotations/path/to/file?at=',
            jsonResponse
        );
        const repo = Stash.connect("localhost", "7990").repo("project_1", "mycode");
        const annotations = repo.annotationsFor("", "/path/to/file");
        annotations.then((json) => {
            deepEqual(jsonResponse, json);
            done();
        }).catch(done.fail);

    });

    it('Fetches raw file.', () => {

    });

    it('Posts annotations for repo', () => {

    });

    it('Fetches navigation info', () => {

    });
});