"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_redux_1 = require("react-redux");
var redux_1 = require("redux");
var React = require("react");
exports.INIT = "INIT";
exports.init = function () {
    return { type: exports.INIT };
};
var mapStateToProps = function (state) { return state; };
var mapDispatchToProps = function (dispatch) { return redux_1.bindActionCreators({}, dispatch); };
exports.App = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(function (props) { return <div>{props.cake}</div>; });
