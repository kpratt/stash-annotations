import { shallow, mount, render } from 'enzyme';
import { createMockStore } from 'redux-test-utils';

import {App} from 'main/app'

// why this works and import does not?
const React = require('react');

describe("App self wires on INIT event", () => {
    it('adds 1 + 2 to equal 3', () => {
        const mockStore = createMockStore({cake: "let them eat cake"});
        const app = mount(<App store={mockStore}/>);
        console.log(app.childAt(0));
        const text = app.text();
        expect(text).toBe("let them eat cake");
    });
});