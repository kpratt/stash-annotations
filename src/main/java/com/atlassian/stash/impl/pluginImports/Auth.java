package com.atlassian.stash.impl.pluginImports;

import com.atlassian.bitbucket.AuthorisationException;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.stash.Servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class Auth implements com.atlassian.stash.Auth {
    private static final Logger LOGGER = LoggerFactory.getLogger(Auth.class);

    private final PermissionService permissionService;
    private final PermissionValidationService permissionValidationService;

    @Autowired
    public Auth(
            PermissionService permissionService,
            PermissionValidationService permissionValidationService
    ) {
        this.permissionService = permissionService;
        this.permissionValidationService = permissionValidationService;
    }

    public Servlet around(final Servlet innerServlet) {
        return new Servlet() {
            public void handle(HttpServletRequest request, HttpServletResponse response) throws IOException {
                try {
                    permissionValidationService.validateAuthenticated();
                    innerServlet.handle(request, response);
                } catch (AuthorisationException e) {
                    handle403(response);
                }
            }
        };
    }

    private void handle403(HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setStatus(403);
        response.getWriter()
                .append("<html>")
                .append("<head><title>403 Not authorised</title></head>")
                .append("<body><h1>Not Authorized</h1></body>")
                .append("</html>")
                .close();
    }
}
