"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var client_1 = require("main/stash/client");
var assert_1 = require("assert");
describe("Stash Client", function () {
    afterEach(fetch.restore);
    it('Fetch is mocked.', function (done) {
        var jsonResponse = {
            cake: 'eat it you will',
        };
        fetch.get('https://localhost', {
            status: 200,
            body: jsonResponse,
        });
        fetch('https://localhost').
            then(function (resp) { return resp.json(); }).
            then(function (json) {
            assert_1.deepEqual(jsonResponse, json);
            done();
        }).catch(done.fail);
    });
    it('Fetches annotations for repo file.', function (done) {
        var jsonResponse = {
            cake: 'eat it you will',
        };
        fetch.get('https://localhost:7990/bitbucket/plugins/servlet/annotated-view/projects/project_1/repo/mycode/annotations/path/to/file?at=', jsonResponse);
        var repo = client_1.default.connect("localhost", "7990").repo("project_1", "mycode");
        var annotations = repo.annotationsFor("", "/path/to/file");
        annotations.then(function (json) {
            assert_1.deepEqual(jsonResponse, json);
            done();
        }).catch(done.fail);
    });
    it('Fetches raw file.', function () {
    });
    it('Posts annotations for repo', function () {
    });
    it('Fetches navigation info', function () {
    });
});
