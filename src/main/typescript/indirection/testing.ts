import { Environment } from "./environment";
import { TestScheduler } from 'rxjs';
import { deepEqual } from 'assert';

export const env: Environment = {
    rxScheduler: new TestScheduler(deepEqual),
};

export default env;