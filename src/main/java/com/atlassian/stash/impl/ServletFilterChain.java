package com.atlassian.stash.impl;

import com.atlassian.stash.Servlet;
import com.atlassian.stash.ServletFilter;
import com.google.common.collect.ImmutableList;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class ServletFilterChain {


    public static ServletFilterChain of(ServletFilter filter) {
        return new ServletFilterChain(ImmutableList.<ServletFilter>of(filter));
    }

    private final List<ServletFilter> filters;

    private ServletFilterChain(List<ServletFilter> filters) {
        this.filters = filters;
    }

    public ServletFilterChain around(ServletFilter filter) {
        LinkedList<ServletFilter> list = new LinkedList<ServletFilter>();
        list.addAll(filters);
        list.add(filter);
        return new ServletFilterChain(
                Collections.unmodifiableList(list)
        );
    }

    public com.atlassian.stash.Servlet build(com.atlassian.stash.Servlet servlet) {
        Stack<ServletFilter> stack = new Stack<ServletFilter>();
        for (ServletFilter filter : filters) {
            stack.push(filter);
        }
        Servlet ret = servlet;
        while(!stack.empty()){
            ret = stack.pop().around(ret);
        }
        return ret;
    }
}
