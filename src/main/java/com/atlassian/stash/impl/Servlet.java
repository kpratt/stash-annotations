package com.atlassian.stash.impl;

import com.atlassian.stash.Auth;
import com.atlassian.stash.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// http://localhost:7990/bitbucket/plugins/servlet/annotated/*
public class Servlet extends HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(Servlet.class);

    private final ServletFilterChain commonFilters;
    private final Router router;

    @Autowired
    public Servlet(
            AccessLogger accessLogger, // log all access attempts
            Auth authenticator,        // logged in users only
            UIProvider uiProvider      // if they don't want json serve the ui
    ) {
        commonFilters = ServletFilterChain.
                of(accessLogger).
                around(authenticator).
                around(uiProvider);
        router = new Router().
                register(Router.Method.GET, "/annotated-view/", commonFilters.build(UIProvider::render));
    }

    private void doRoute(
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        try {
            router.handle(request, response);
        } catch (IOException e) {
            LOGGER.error("Uncaught error {}", e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doRoute(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        doRoute(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) {
        doRoute(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) {
        doRoute(request, response);
    }
}
