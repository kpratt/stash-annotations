import * as React from "react";
import * as ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {createStore} from "redux";
import rootReducer from "./reducers";
import {App, init} from "./app"



window.onload = () => {
    const store = createStore(rootReducer);
    ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider>,
        document.getElementById("body")
    );
    store.dispatch(init())
};
