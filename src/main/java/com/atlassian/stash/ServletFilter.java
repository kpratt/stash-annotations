package com.atlassian.stash;

import com.atlassian.stash.Servlet;

public interface ServletFilter {
    Servlet around(Servlet inner);
}
