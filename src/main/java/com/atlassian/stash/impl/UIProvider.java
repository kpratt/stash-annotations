package com.atlassian.stash.impl;

import com.atlassian.stash.Servlet;
import com.atlassian.stash.ServletFilter;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * In the event they don't explicitly ask for JSON, then
 * we should load the single page app.
 */
@Component
public class UIProvider implements ServletFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(com.atlassian.stash.impl.Servlet.class);

    @Override
    public Servlet around(Servlet inner) {
        return (HttpServletRequest request, HttpServletResponse response) -> {
            if("application/json".equals(request.getHeader("Accept"))) {
                inner.handle(request, response);
            } else {
                render(request, response);
            }
        };
    }

    public static void render(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        InputStream frontend = UIProvider.class.getResourceAsStream("/scripts/source-viewer-annotations");
        if (frontend == null) {
            resp.setStatus(500);
            resp.getWriter().close();
            LOGGER.error("Unable to locate frontend batch file.");
            return;
        }
        resp.setStatus(200);
        resp.setContentType("text/html;charset=UTF-8");

        resp.getWriter().append("<html><head><title></title><script type=\"application/javascript\">");
        IOUtils.copy(frontend, resp.getWriter());
        resp.getWriter().append("</script></head><body><div id=\"body\"></div></body></html>");
        resp.getWriter().close();
    }
}
