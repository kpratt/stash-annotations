package com.atlassian.stash;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Router implements Servlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(Router.class);



    public enum Method {
        GET,
        POST,
        PUT,
        DELETE
    }

    static class Routing implements Servlet {
        final String path;
        final Servlet servlet;

        public Routing(String path, Servlet servlet) {
            this.path = path;
            this.servlet = servlet;
        }

        public boolean matches(String servletPath) {
            return path.equals(servletPath);
        }

        @Override
        public void handle(HttpServletRequest request, HttpServletResponse response) throws IOException {
            servlet.handle(request, response);
        }
    }

    private final Map<String, List<Routing>> routes;
    public Router() {
        routes = new ConcurrentHashMap<>();
        routes.put("GET", new ArrayList<>());
        routes.put("POST", new ArrayList<>());
        routes.put("PUT", new ArrayList<>());
        routes.put("DELETE", new ArrayList<>());
    }

    public Router register(final Method method, final String path, final Servlet servlet) {
        routes.get(method.name()).add(new Routing(path, servlet));
        return this;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response) throws IOException {
        List<Routing> mRoutes = routes.get(request.getMethod().toUpperCase());
        for(Routing r : mRoutes) {
            if(r.matches(request.getServletPath())) {
                r.handle(request, response);
                return;
            }
        }
        LOGGER.error("No route matched {} {}", request.getMethod(), request.getServletPath());
        response.setStatus(500);
        response.getWriter().close();
    }
}
