import {AsyncScheduler} from "rxjs/scheduler/AsyncScheduler";

export type Environment = {
    rxScheduler: AsyncScheduler,
}

import { Environment } from "./environment";
import { Scheduler} from 'rxjs';

const env: Environment = {
    rxScheduler: Scheduler.async,
};

export default env;

