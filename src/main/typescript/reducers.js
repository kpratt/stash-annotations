"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
var rootReducer = redux_1.combineReducers({
    noop: noop
});
exports.initialState = { cake: "let them eat it" };
function noop(state, action) {
    if (state === void 0) { state = exports.initialState; }
    return state || exports.initialState || { cake: "is bad m'kay" };
}
exports.default = rootReducer;
