import {connect, Dispatch} from "react-redux";
import {State} from "./reducers"
import {bindActionCreators} from "redux";
import * as React from "react";

export const INIT = "INIT";

export const init = () => {
    return {type: INIT}
};

const mapStateToProps = (state: State): State => state;

const mapDispatchToProps =
    (dispatch: Dispatch<State>) =>
        bindActionCreators({}, dispatch);

export const App = connect(
    mapStateToProps,
    mapDispatchToProps
)(
    (props: State) =>
        <div>{props.cake}</div>);

