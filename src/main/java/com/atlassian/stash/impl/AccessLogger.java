package com.atlassian.stash.impl;

import com.atlassian.stash.Servlet;
import com.atlassian.stash.ServletFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AccessLogger implements ServletFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(com.atlassian.stash.impl.Servlet.class);

    @Override
    public Servlet around(Servlet inner) {
        return (HttpServletRequest request, HttpServletResponse response) -> {
            LOGGER.info("Access request for {}", request.getServletPath());
            try {
                inner.handle(request, response);
            } catch (Exception e) {
                LOGGER.error("Servlet error:", e);
            }
        };
    }
}
