"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var enzyme_1 = require("enzyme");
var redux_test_utils_1 = require("redux-test-utils");
var app_1 = require("main/app");
// why this works and import does not?
var React = require('react');
describe("App self wires on INIT event", function () {
    it('adds 1 + 2 to equal 3', function () {
        var mockStore = redux_test_utils_1.createMockStore({ cake: "let them eat cake" });
        var app = enzyme_1.mount(<app_1.App store={mockStore}/>);
        console.log(app.childAt(0));
        var text = app.text();
        expect(text).toBe("let them eat cake");
    });
});
